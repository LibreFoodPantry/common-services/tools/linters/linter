# .linter

A template for linter gitlab-ci.yml files.

Linters can inherit from this by adding 
`extends: https://gitlab.com/LibreFoodPantry/common-services/tools/linters/linter/-/raw/main/.linter.yaml`
to their gitlab-ci.yml file.

They will inherit:

- `stage: lint`
- `needs: []` which allows the linter to run in the pipeline without waiting
for any other stage(s) to complete.

## Licensing

* Code is licensed under [GPL 3.0](documentation/licenses/gpl-3.0.txt).
* Content is licensed under
  [CC-BY-SA 4.0](documentation/licenses/cc-by-sa-4.0.md).
* Developers sign-off on [DCO 1.0](documentation/licenses/dco.txt)
  when they make contributions.
